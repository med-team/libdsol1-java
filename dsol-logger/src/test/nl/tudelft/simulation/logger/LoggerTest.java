/*
 * @(#) LoggerTest.java Sep 28, 2004 Copyright (c) 2002-2005 Delft University of
 * Technology Jaffalaan 5, 2628 BX Delft, the Netherlands. All rights reserved.
 * This software is proprietary information of Delft University of Technology
 * The code is published under the Lesser General Public License
 */
package nl.tudelft.simulation.logger;

import junit.framework.TestCase;

/**
 * <p>
 * (c) copyright 2002-2005 <a href="http://www.simulation.tudelft.nl">Delft
 * University of Technology </a>, the Netherlands. <br>
 * See for project information <a
 * href="http://www.simulation.tudelft.nl">www.simulation.tudelft.nl </a> <br>
 * License of use: <a href="http://www.gnu.org/copyleft/lesser.html">Lesser
 * General Public License (LGPL) </a>, no warranty.
 * 
 * @author <a href="http://www.peter-jacobs.com/index.htm">Peter Jacobs </a>
 * @version 1.2 Sep 28, 2004
 * @since 1.5
 */
public class LoggerTest extends TestCase
{

    /**
     * constructs a new LoggerTest
     */
    public LoggerTest()
    {
        this("test");
    }

    /**
     * constructs a new LoggerTest
     * 
     * @param arg0
     */
    public LoggerTest(String arg0)
    {
        super(arg0);
    }

    /**
     * tests the Logger
     */
    public void test()
    {
        // TODO fill in the test
    }
}