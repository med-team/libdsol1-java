/*
 * @(#) Euler.java Apr 20, 2004 Copyright (c) 2002-2005 Delft University of
 * Technology Jaffalaan 5, 2628 BX Delft, the Netherlands. All rights reserved.
 * This software is proprietary information of Delft University of Technology
 * The code is published under the Lesser General Public License
 */
package nl.tudelft.simulation.jstats.ode.integrators;

import nl.tudelft.simulation.jstats.ode.DifferentialEquationInterface;

/**
 * The Euler numerical estimator as described in <a
 * href="http://mathworld.wolfram.com/EulerForwardMethod.html">
 * http://mathworld.wolfram.com/EulerForwardMethod.html </a>
 * <p>
 * (c) copyright 2002-2005 <a href="http://www.simulation.tudelft.nl">Delft
 * University of Technology </a>, the Netherlands. <br>
 * See for project information <a
 * href="http://www.simulation.tudelft.nl">www.simulation.tudelft.nl </a> <br>
 * License of use: <a href="http://www.gnu.org/copyleft/lesser.html">Lesser
 * General Public License (LGPL) </a>, no warranty.
 * 
 * @author <a href="http://www.peter-jacobs.com/index.htm">Peter Jacobs </a>
 * @version $Revision: 1.8 $ $Date: 2005/08/09 10:41:11 $
 * @since 1.5
 */
public class Euler extends NumericalIntegrator
{
    /**
     * constructs a new Euler
     * 
     * @param timeStep the timeStep
     * @param equation the differentialEquation
     */
    public Euler(final double timeStep,
            final DifferentialEquationInterface equation)
    {
        super(timeStep, equation);
    }

    /**
     * @see nl.tudelft.simulation.jstats.ode.integrators.NumericalIntegrator#next(double,
     *      double[])
     */
    @Override
    public double[] next(final double x, final double[] y)
    {
        return super.add(y, super.multiply(this.timeStep, this.equation
                .dy(x, y)));
    }
}