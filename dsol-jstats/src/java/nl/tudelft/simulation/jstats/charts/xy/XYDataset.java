/*
 * @(#) XYDataset.java Sep 26, 2003 Copyright (c) 2002-2005 Delft University of
 * Technology Jaffalaan 5, 2628 BX Delft, the Netherlands. All rights reserved.
 * This software is proprietary information of Delft University of Technology
 * The code is published under the Lesser General Public License
 */
package nl.tudelft.simulation.jstats.charts.xy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import nl.tudelft.simulation.language.filters.Filterinterface;

import org.jfree.data.AbstractDataset;
import org.jfree.data.DatasetChangeEvent;
import org.jfree.data.DatasetChangeListener;

/**
 * The xyDataset specifies the xyDataset in DSOL
 * <p>
 * (c) copyright 2002-2005 <a href="http://www.simulation.tudelft.nl">Delft
 * University of Technology </a>, the Netherlands. <br>
 * See for project information <a href="http://www.simulation.tudelft.nl">
 * www.simulation.tudelft.nl </a> <br>
 * License of use: <a href="http://www.gnu.org/copyleft/lesser.html">Lesser
 * General Public License (LGPL) </a>, no warranty.
 * 
 * @author <a href="http://www.peter-jacobs.com">Peter Jacobs </a>
 * @version $Revision: 1.9 $ $Date: 2005/07/04 12:17:34 $
 * @since 1.5
 */
public class XYDataset extends AbstractDataset implements
        org.jfree.data.XYDataset, DatasetChangeListener
{

    /** series contains the series of the set */
    private XYSeries[] series = new XYSeries[0];

    /**
     * constructs a new XYDataset
     */
    public XYDataset()
    {
        super();
        this.fireDatasetChanged();
    }

    /**
     * @see org.jfree.data.DatasetChangeListener
     *      #datasetChanged(org.jfree.data.DatasetChangeEvent)
     */
    public void datasetChanged(final DatasetChangeEvent arg0)
    {
        if (arg0 != null)
        {
            this.fireDatasetChanged();
        }
    }

    /**
     * adds a dataset to the series
     * 
     * @param dataset the set
     */
    public synchronized void addSeries(final XYSeries dataset)
    {
        dataset.addChangeListener(this);
        List<XYSeries> list = new ArrayList<XYSeries>(Arrays
                .asList(this.series));
        list.add(dataset);
        this.series = list.toArray(new XYSeries[list.size()]);
        this.fireDatasetChanged();
    }

    /**
     * @see org.jfree.data.XYDataset#getItemCount(int)
     */
    public int getItemCount(final int serie)
    {
        return this.series[serie].getItemCount();
    }

    /**
     * @see org.jfree.data.XYDataset#getXValue(int, int)
     */
    public Number getXValue(final int serie, final int item)
    {
        return this.series[serie].getXValue(item);
    }

    /**
     * @see org.jfree.data.XYDataset#getYValue(int, int)
     */
    public Number getYValue(final int serie, final int item)
    {
        return this.series[serie].getYValue(item);
    }

    /**
     * @see org.jfree.data.SeriesDataset#getSeriesCount()
     */
    public int getSeriesCount()
    {
        return this.series.length;
    }

    /**
     * @see org.jfree.data.SeriesDataset#getSeriesName(int)
     */
    public String getSeriesName(final int serie)
    {
        return this.series[serie].getSeriesName();
    }

    /**
     * @see org.jfree.data.XYDataset#getX(int, int)
     */
    public double getX(final int serie, final int item)
    {
        return this.series[serie].getXValue(item).doubleValue();
    }

    /**
     * @see org.jfree.data.XYDataset#getY(int, int)
     */
    public double getY(final int serie, final int item)
    {
        return this.series[serie].getYValue(item).doubleValue();
    }

    /**
     * applies a filter on the chart
     * 
     * @param filter the filter to apply
     */
    public void setFilter(final Filterinterface filter)
    {
        for (int i = 0; i < this.series.length; i++)
        {
            this.series[i].setFilter(filter);
        }
    }
}