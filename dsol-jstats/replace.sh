#!/bin/bash

for file in $( find ./ -type f -name '*.java')
do
  echo $file  # Each file
  sed -i "s/www.tbm.tudelft.nl\/webstaf\/peterja/www.peter-jacobs.com/g" $file
done
exit 0
