#!/bin/sh

set -e
NAME=`dpkg-parsechangelog | awk '/^Source/ { print $2 }'`
VERSION=`dpkg-parsechangelog | awk '/^Version/ { print $2 }' | sed -e 's/^[0-9]*://' -e 's/-.*//'`

if ! [ -e debian/changelog ] ; then
    echo "Run this from the source directory"
    exit 1
fi

mkdir -p ../tarballs
cd ../tarballs

rm -rf ${NAME}-${VERSION}
mkdir ${NAME}-${VERSION}
cd ${NAME}-${VERSION}

cvs -d :pserver:anonymous@dsol.cvs.sourceforge.net:/cvsroot/dsol login 
cvs -z3 -d:pserver:anonymous@dsol.cvs.sourceforge.net:/cvsroot/dsol export -r HEAD -d dsol-language language
cvs -z3 -d:pserver:anonymous@dsol.cvs.sourceforge.net:/cvsroot/dsol export -r HEAD -d dsol-event event 
cvs -z3 -d:pserver:anonymous@dsol.cvs.sourceforge.net:/cvsroot/dsol export -r HEAD -d dsol-logger logger
cvs -z3 -d:pserver:anonymous@dsol.cvs.sourceforge.net:/cvsroot/dsol export -r HEAD -d dsol-naming naming
cvs -z3 -d:pserver:anonymous@dsol.cvs.sourceforge.net:/cvsroot/dsol export -r HEAD -d dsol-jstats jstats
cvs -z3 -d:pserver:anonymous@dsol.cvs.sourceforge.net:/cvsroot/dsol export -r HEAD -d dsol-interpreter interpreter
cvs -z3 -d:pserver:anonymous@dsol.cvs.sourceforge.net:/cvsroot/dsol export -r HEAD -d dsol-introspection introspection

find . -type f -name "*.jar" -delete
find . -type f -name ".cvs*" -delete
cd ..
tar --owner=root --group=root --mode=a+rX -caf ${NAME}_${VERSION}.orig.tar.xz ${NAME}-${VERSION}
mv ${NAME}-${VERSION} ${NAME}-${VERSION}.orig
