/*
 * @(#) Exec.java Feb 1, 2005 Copyright (c) 2004 Delft University of Technology
 * Jaffalaan 5, 2628 BX Delft, the Netherlands All rights reserved. This
 * software is proprietary information of Delft University of Technology The
 * code is published under the General Public License
 */
package nl.tudelft.simulation.dsol.process;

import nl.tudelft.simulation.dsol.interpreter.process.Process;

/**
 * The specifies
 * <p>
 * (c) copyright 2004 <a href="http://www.simulation.tudelft.nl/dsol/">Delft
 * University of Technology </a>, the Netherlands. <br>
 * See for project information <a href="http://www.simulation.tudelft.nl/dsol/">
 * www.simulation.tudelft.nl/dsol </a> <br>
 * License of use: <a href="http://www.gnu.org/copyleft/gpl.html">General Public
 * License (GPL) </a>, no warranty <br>
 * 
 * @author <a href="http://www.peter-jacobs.com/index.htm"> Peter Jacobs </a>
 * @version $Revision: 1.3 $ $Date: 2005/07/04 12:44:29 $
 * @since 1.5
 */
public class Exec
{
    /**
     * constructs a new Exec
     */
    public Exec()
    {
        super();
    }

    /**
     * @param process
     */
    private static void elaborate(Process process)
    {
        process.resume();
    }

    /**
     * @param args
     */
    public static void main(String[] args)
    {
        // elaborate(new Cow());
        elaborate(new Dog());
    }

}
