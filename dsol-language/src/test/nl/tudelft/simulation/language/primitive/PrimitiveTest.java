/*
 * @(#) MonitorTest.java Sep 28, 2004 Copyright (c) 2002-2005 Delft University
 * of Technology Jaffalaan 5, 2628 BX Delft, the Netherlands. All rights
 * reserved. This software is proprietary information of Delft University of
 * Technology The code is published under the Lesser General Public License
 */
package nl.tudelft.simulation.language.primitive;

import junit.framework.Assert;
import junit.framework.TestCase;
import nl.tudelft.simulation.language.primitives.Primitive;

/**
 * The JUNIT Test for the <code>Primitive</code>.
 * <p>
 * (c) copyright 2002-2005 <a href="http://www.simulation.tudelft.nl">Delft
 * University of Technology </a>, the Netherlands.
 * <p>
 * See for project information <a
 * href="http://www.simulation.tudelft.nl/dsol/language">www.simulation.tudelft.nl/language
 * </a> <br>
 * License of use: <a href="http://www.gnu.org/copyleft/lesser.html">Lesser
 * General Public License (LGPL) </a>, no warranty
 * 
 * @author <a href="http://www.peter-jacobs.com/index.htm">Peter Jacobs </a>
 * @version 1.2 Sep 28, 2004
 * @since 1.5
 */
public class PrimitiveTest extends TestCase
{

    /**
     * constructs a new PrimitiveTest
     */
    public PrimitiveTest()
    {
        this("test");
    }

    /**
     * constructs a new PrimitiveTest
     * 
     * @param arg0
     */
    public PrimitiveTest(String arg0)
    {
        super(arg0);
    }

    /**
     * tests the Primitive
     */
    public void test()
    {
        Assert.assertEquals(Primitive.getPrimitive(Boolean.class),
                boolean.class);
        Assert.assertEquals(Primitive.getPrimitive(Integer.class), int.class);
        Assert.assertEquals(Primitive.getPrimitive(Double.class), double.class);
        Assert.assertEquals(Primitive.getPrimitive(Float.class), float.class);
        Assert
                .assertEquals(Primitive.getPrimitive(Character.class),
                        char.class);
        Assert.assertEquals(Primitive.getPrimitive(Byte.class), byte.class);
        Assert.assertEquals(Primitive.getPrimitive(Short.class), short.class);
        Assert.assertEquals(Primitive.getPrimitive(Long.class), long.class);

        Assert.assertEquals(Primitive.getWrapper(boolean.class), Boolean.class);
        Assert.assertEquals(Primitive.getWrapper(int.class), Integer.class);
        Assert.assertEquals(Primitive.getWrapper(double.class), Double.class);
        Assert.assertEquals(Primitive.getWrapper(float.class), Float.class);
        Assert.assertEquals(Primitive.getWrapper(char.class), Character.class);
        Assert.assertEquals(Primitive.getWrapper(byte.class), Byte.class);
        Assert.assertEquals(Primitive.getWrapper(long.class), Long.class);

        Assert.assertEquals(Primitive.toBoolean(new Integer(1)), Boolean.TRUE);
        Assert.assertEquals(Primitive.toBoolean(new Double(1.0)), Boolean.TRUE);
        Assert.assertEquals(Primitive.toBoolean(new Float(1.0)), Boolean.TRUE);
        Assert.assertEquals(Primitive.toBoolean(new Short((short) 1.0)),
                Boolean.TRUE);
        Assert.assertEquals(Primitive.toBoolean(new Long(1l)), Boolean.TRUE);

        Assert.assertEquals(Primitive.toBoolean(new Integer(0)), Boolean.FALSE);
        Assert
                .assertEquals(Primitive.toBoolean(new Double(0.0)),
                        Boolean.FALSE);
        Assert.assertEquals(Primitive.toBoolean(new Float(0.0)), Boolean.FALSE);
        Assert.assertEquals(Primitive.toBoolean(new Short((short) 0.0)),
                Boolean.FALSE);
        Assert.assertEquals(Primitive.toBoolean(new Long(0l)), Boolean.FALSE);
    }
}