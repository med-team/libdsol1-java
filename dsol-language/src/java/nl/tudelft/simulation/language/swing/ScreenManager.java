/*
 * @(#) ScreenManager.java Apr 29, 2004 Copyright (c) 2002-2005 Delft University
 * of Technology Jaffalaan 5, 2628 BX Delft, the Netherlands. All rights
 * reserved. This software is proprietary information of Delft University of
 * Technology The code is published under the Lesser General Public License
 */
package nl.tudelft.simulation.language.swing;

import java.awt.DisplayMode;
import java.awt.GraphicsEnvironment;
import java.awt.Window;

import javax.swing.JFrame;

/**
 * The ScreenManager class manages initializing and displaying full screen
 * graphics mode.
 * <p>
 * (c) copyright 2002-2005 <a href="http://www.simulation.tudelft.nl">Delft
 * University of Technology </a>, the Netherlands.
 * <p>
 * See for project information <a
 * href="http://www.simulation.tudelft.nl/dsol/language">www.simulation.tudelft.nl/language
 * </a> <br>
 * License of use: <a href="http://www.gnu.org/copyleft/lesser.html">Lesser
 * General Public License (LGPL) </a>, no warranty
 * 
 * @version $Revision: 1.8 $ $Date: 2005/08/11 05:49:12 $
 * @author <a href="mailto:stijnh@tbm.tudelft.nl">
 *         Stijn-Pieter van Houten </a>
 */
public class ScreenManager
{
    /** the environment */
    private GraphicsEnvironment environment;

    /**
     * Constructs a new ScreenManager.
     */
    public ScreenManager()
    {
        super();
        this.environment = GraphicsEnvironment.getLocalGraphicsEnvironment();
    }

    /**
     * Method setFullScreen. Enters full screen mode and changes the display
     * mode.
     * 
     * @param window The window to show full screen.
     */
    public void setFullScreen(final JFrame window)
    {
        window.setUndecorated(true);
        window.setResizable(false);
        this.environment.getDefaultScreenDevice().setFullScreenWindow(window);
        if (this.environment.getDefaultScreenDevice()
                .isDisplayChangeSupported())
        {
            DisplayMode mode = new DisplayMode((int) this.environment
                    .getMaximumWindowBounds().getWidth(),
                    (int) this.environment.getMaximumWindowBounds().getWidth(),
                    24, DisplayMode.REFRESH_RATE_UNKNOWN);
            this.environment.getDefaultScreenDevice().setDisplayMode(mode);

        }
    }

    /**
     * Method getFullScreenWindow.
     * 
     * @return Returns the window currently used in full screen mode.
     */
    public Window getFullScreenWindow()
    {
        return this.environment.getDefaultScreenDevice().getFullScreenWindow();
    }

    /**
     * Method restoreScreen. Restores the screen's display mode.
     */
    public void restoreScreen()
    {
        Window window = this.environment.getDefaultScreenDevice()
                .getFullScreenWindow();
        if (window != null)
        {
            window.dispose();
        }
        this.environment.getDefaultScreenDevice().setFullScreenWindow(null);
    }
}