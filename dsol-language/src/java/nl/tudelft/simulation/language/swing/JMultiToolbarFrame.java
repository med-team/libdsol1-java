/*
 * @(#) JMultiToolbarFrame.java 20-jul-2004 Copyright (c) 2002-2005 Delft
 * University of Technology Jaffalaan 5, 2628 BX Delft, the Netherlands. All
 * rights reserved. This software is proprietary information of Delft University
 * of Technology The code is published under the Lesser General Public License
 */
package nl.tudelft.simulation.language.swing;

import java.awt.BorderLayout;
import java.awt.Container;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;

/**
 * JMultiToolbarFrame.java .
 * <p>
 * (c) copyright 2002-2005 <a href="http://www.simulation.tudelft.nl">Delft
 * University of Technology </a>, the Netherlands.
 * <p>
 * See for project information <a
 * href="http://www.simulation.tudelft.nl/dsol/language">www.simulation.tudelft.nl/language
 * </a> <br>
 * License of use: <a href="http://www.gnu.org/copyleft/lesser.html">Lesser
 * General Public License (LGPL) </a>, no warranty
 * 
 * @version $Revision: 1.9 $ $Date: 2005/08/11 05:49:12 $
 * @author <a href="mailto:royc@tbm.tudelft.nl">Roy Chin
 *         </a>
 */
public class JMultiToolbarFrame extends JFrame implements SwingConstants
{
    /**
     * The pane you can fiddle with -- to which content can be added, of which
     * the layout can be changed, and so on. It's returned by <TT>
     * getContentPane()</TT>.
     */
    protected Container currentContentPane;

    /**
     * A stack of all the toolbar containers -- those "outside" of <TT>
     * currentContentPane</TT>. The indices match those of <TT>toolbarStack
     * </TT>.
     */
    protected Vector<Container> containerStack;

    /**
     * A stack of all the toolbars. The indices match those of <TT>
     * containerStack</TT>.
     */
    protected Vector<JToolBar> toolbarStack;

    /**
     * The container of <TT>currentContentPane</TT>.
     */
    protected Container topContainer;

    /**
     * Whether the window is empty (i.e., has only an unedited, blank 'untitled'
     * document in it).
     */
    protected boolean isEmpty;

    // ------------------------------------------------------------
    // Constructors
    // ------------------------------------------------------------

    /**
     * Returns a new <TT>JMultiToolbarFrame</TT>.
     */
    public JMultiToolbarFrame()
    {
        this.isEmpty = true;
        this.containerStack = new Vector<Container>();
        this.toolbarStack = new Vector<JToolBar>();
        this.currentContentPane = new JPanel(new BorderLayout());
        this.topContainer = super.getContentPane();
        this.topContainer.add(this.currentContentPane, BorderLayout.CENTER);
    }

    // ------------------------------------------------------------
    // Public instance methods
    // ------------------------------------------------------------

    /**
     * Returns whether the window is empty.
     * 
     * @return isEmpty
     */
    public boolean isEmpty()
    {
        return this.isEmpty;
    }

    /**
     * Sets whether the window is empty
     * 
     * @param empty Empty frame or not
     */
    public void setEmpty(final boolean empty)
    {
        this.isEmpty = empty;
    }

    /**
     * Overrides <TT>JFrame.getContentPane()</TT>. The pane returned should
     * always be "inside" the toolbars.
     * 
     * @return The content pane.
     */
    @Override
	public Container getContentPane()
    {
        return this.currentContentPane;
    }

    /**
     * Adds another toolbar to the specified side of the frame. Any attempt to
     * add a toolbar that has already been added is ignored.
     * 
     * @param bar The toolbar to add.
     * @param align One of <TT>SwingConstants.TOP</TT>,<TT>
     *        SwingConstants.BOTTOM</TT>,<TT>SwingConstants.LEFT</TT>, and
     *        <TT>SwingConstants.RIGHT</TT>.
     */
    public void addJToolBar(final JToolBar bar, final int align)
    {
        if (!this.toolbarStack.contains(bar))
        {
            String border;
            int orientation;
            switch (align)
            {
                case TOP:
                    border = BorderLayout.NORTH;
                    orientation = HORIZONTAL;
                    break;
                case BOTTOM:
                    border = BorderLayout.SOUTH;
                    orientation = HORIZONTAL;
                    break;
                case LEFT:
                    border = BorderLayout.WEST;
                    orientation = VERTICAL;
                    break;
                case RIGHT:
                    border = BorderLayout.EAST;
                    orientation = VERTICAL;
                    break;
                default:
                    throw new IllegalArgumentException(
                            "Alignment argument passed to JMultiToolbarFrame.addJToolBar is not one of SwingConstants.TOP/BOTTOM/LEFT/RIGHT.");
            }

            /*
             * The current content pane is lifted from the top container and
             * another container, containing the new toolbar, is inserted
             * between them. The new container and new toolbar are added to the
             * respective stacks, and the new container becomes the new top
             * container. Note that the original content pane
             * (super.getContentPane()) does act as a top container, but never
             * contains a toolbar and is never added to the container stack. The
             * current content pane always stays on top.
             */
            this.topContainer.remove(this.currentContentPane);
            Container newContainer = new JPanel(new BorderLayout());
            newContainer.add(bar, border);
            newContainer.add(this.currentContentPane, BorderLayout.CENTER);
            this.topContainer.add(newContainer);
            this.topContainer = newContainer;
            this.containerStack.addElement(newContainer);
            bar.setOrientation(orientation);
            this.toolbarStack.addElement(bar);
        }
    }

    /**
     * Removes the specified toolbar. Any attempt to remove a toolbar that has
     * never been added is ignored.
     * 
     * @param bar Toolbar to remove
     */
    public void removeJToolBar(final JToolBar bar)
    {
        if (this.toolbarStack.contains(bar))
        {
            Container current;
            Container above;
            Container below;
            int position = this.toolbarStack.indexOf(bar);
            if (position != 0)
            {
                below = this.containerStack.elementAt(position - 1);
            } else
            {
                below = super.getContentPane();
            }

            if (position != (this.containerStack.size() - 1))
            {
                above = this.containerStack.elementAt(position + 1);
            } else
            {
                above = this.currentContentPane;
            }

            current = this.containerStack.elementAt(position);
            below.remove(current);
            current.remove(above);
            below.add(above, BorderLayout.CENTER);
        }
    }
}
