/*
 * @(#)Angle.java Jun 13, 2004 Copyright (c) 2002-2005 Delft University of Technology Jaffalaan 5, 2628 BX
 * Delft, the Netherlands. All rights reserved. This software is proprietary information of Delft University
 * of Technology The code is published under the Lesser General Public License
 */
package nl.tudelft.simulation.language.d2;

import java.awt.geom.Point2D;

/**
 * The Angle class presents a number of mathematical utility functions on the angle.
 * <p>
 * (c) copyright 2002-2005 <a href="http://www.simulation.tudelft.nl">Delft University of Technology </a>, the
 * Netherlands.
 * <p>
 * See for project information <a
 * href="http://www.simulation.tudelft.nl/dsol/language">www.simulation.tudelft.nl/language </a> <br>
 * License of use: <a href="http://www.gnu.org/copyleft/lesser.html">Lesser General Public License (LGPL)
 * </a>, no warranty
 * 
 * @version $Revision: 1.8 $ $Date: 2005/08/11 05:49:12 $
 * @author <a href="mailto:a.verbraeck@tbm.tudelft.nl">Alexander Verbraeck </a>
 */
public final class Angle
{
    /**
     * constructs a new Angle.
     */
    private Angle()
    {
        super();
        // unreachable code
    }

    /**
     * Normalize an angle between 0 and 2*pi.
     * 
     * @param angle
     *            the angle to normalize
     * @return normalized angle
     */
    public static double normalize2Pi(final double angle)
    {
        double result = angle + 2.0d * Math.PI;
        double times = Math.floor(result / (2.0d * Math.PI));
        result -= times * 2.0d * Math.PI;
        return result;
    }

    /**
     * Normalize an angle between -pi and +pi.
     * 
     * @param angle
     *            the angle to normalize
     * @return normalized angle
     */
    public static double normalizePi(final double angle)
    {
        double result = angle + 2.0d * Math.PI;
        double times = Math.floor((result + Math.PI) / (2.0d * Math.PI));
        result -= times * 2.0d * Math.PI;
        return result;
    }

    /**
     * Return the 2-pi normalized angle when making an arc from p0 to p1.
     * 
     * @param p0
     *            first point
     * @param p1
     *            second point
     * @return the normalized angle
     */
    public static double angle(final Point2D p0, final Point2D p1)
    {
        return normalize2Pi(Math.atan2(p1.getY() - p0.getY(), p1.getX() - p0.getX()));
    }
}
