/*
 * @(#)Circle.java Jun 13, 2004 Copyright (c) 2002-2005 Delft University of Technology Jaffalaan 5, 2628 BX
 * Delft, the Netherlands. All rights reserved. This software is proprietary information of Delft University
 * of Technology The code is published under the Lesser General Public License
 */
package nl.tudelft.simulation.language.d2;

import java.awt.geom.Point2D;

/**
 * <p>
 * (c) copyright 2002-2005 <a href="http://www.simulation.tudelft.nl">Delft University of Technology </a>, the
 * Netherlands.
 * <p>
 * See for project information <a
 * href="http://www.simulation.tudelft.nl/dsol/language">www.simulation.tudelft.nl/language </a> <br>
 * License of use: <a href="http://www.gnu.org/copyleft/lesser.html">Lesser General Public License (LGPL)
 * </a>, no warranty
 * 
 * @version $Revision: 1.8 $ $Date: 2005/08/11 05:49:12 $
 * @author <a href="mailto:a.verbraeck@tbm.tudelft.nl">Alexander Verbraeck </a>
 */
public final class Circle
{
    /**
     * constructs a new Circle.
     */
    private Circle()
    {
        super();
        // unreachable code
    }

    /**
     * Elegant intersection algorithm from http://astronomy.swin.edu.au/~pbourke/geometry/2circle/.
     * 
     * @param center0
     *            the center of the first circle
     * @param radius0
     *            the radius of the first circle
     * @param center1
     *            the center of the second circle
     * @param radius1
     *            the radius of the second circle
     * @return the intersection
     */
    public static Point2D[] intersection(final Point2D center0, final double radius0, final Point2D center1,
            final double radius1)
    {
        double distance = center0.distance(center1);
        double x0 = center0.getX();
        double x1 = center1.getX();
        double y0 = center0.getY();
        double y1 = center1.getY();
        Point2D[] result;
        if ((distance > radius0 + radius1) || (distance < Math.abs(radius0 - radius1)))
        {
            return new Point2D.Double[0];
        }
        double a = (radius0 * radius0 - radius1 * radius1 + distance * distance) / (2 * distance);
        double h = Math.sqrt(radius0 * radius0 - a * a);
        double x2 = x0 + ((a / distance) * (x1 - x0));
        double y2 = y0 + ((a / distance) * (y1 - y0));
        if (distance == radius0 + radius1)
        {
            result = new Point2D.Double[1];
            result[0] = new Point2D.Double(x2, y2);
        }
        else
        {
            result = new Point2D.Double[2];
            result[0] = new Point2D.Double(x2 + h * (y1 - y0) / distance, y2 - h * (x1 - x0) / distance);
            result[1] = new Point2D.Double(x2 - h * (y1 - y0) / distance, y2 + h * (x1 - x0) / distance);
        }
        return result;
    }
}
