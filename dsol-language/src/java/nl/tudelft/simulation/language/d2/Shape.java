/*
 * @(#) Shape.java Jun 17, 2004 Copyright (c) 2002-2005 Delft University of Technology Jaffalaan 5, 2628 BX
 * Delft, the Netherlands. All rights reserved. This software is proprietary information of Delft University
 * of Technology The code is published under the Lesser General Public License
 */
package nl.tudelft.simulation.language.d2;

import java.awt.geom.Rectangle2D;

/**
 * Shape utilities.
 * <p>
 * (c) copyright 2002-2005 <a href="http://www.simulation.tudelft.nl">Delft University of Technology </a>, the
 * Netherlands.
 * <p>
 * See for project information <a
 * href="http://www.simulation.tudelft.nl/dsol/language">www.simulation.tudelft.nl/language </a> <br>
 * License of use: <a href="http://www.gnu.org/copyleft/lesser.html">Lesser General Public License (LGPL)
 * </a>, no warranty
 * 
 * @author <a href="http://www.peter-jacobs.com/index.htm">Peter Jacobs </a>
 * @version $Revision: 1.8 $ $Date: 2005/08/11 05:49:12 $
 * @since 1.5
 */
public final class Shape
{
    /**
     * constructs a new Shape.
     */
    private Shape()
    {
        super();
    }

    /**
     * overlaps extent and shape. Overlap = Intersect + Contain
     * 
     * @param extent
     *            the extent
     * @param shape
     *            the shape
     * @return whether extent overlaps shape
     */
    public static boolean overlaps(final Rectangle2D extent, final Rectangle2D shape)
    {
        if (extent.getMaxX() < shape.getMinX())
        {
            return false;
        }
        if (extent.getMaxY() < shape.getMinY())
        {
            return false;
        }
        if (extent.getMinX() > shape.getMaxX())
        {
            return false;
        }
        if (extent.getMinY() > shape.getMaxY())
        {
            return false;
        }
        return true;
    }

    /**
     * @param r1
     *            the first rectangle
     * @param r2
     *            the second rectangle
     * @return whether r1 intersects r2
     */
    public static boolean intersects(final Rectangle2D r1, final Rectangle2D r2)
    {
        return !Shape.contains(r1, r2) && Shape.intersects(r1, r2);
    }

    /**
     * is r1 completely in r2.
     * 
     * @param r1
     *            the first rectangle
     * @param r2
     *            the second rectangle
     * @return whether r1 in r2
     */
    public static boolean contains(final Rectangle2D r1, final Rectangle2D r2)
    {
        boolean contains = (r1.getMinX() <= r1.getMinX() && r1.getMinY() <= r2.getMinY()
                && r1.getMaxX() >= r2.getMaxX() && r1.getMaxY() >= r2.getMaxY());
        return contains;
    }
}
