/*
 * @(#) DirectionalShape.java 23-jul-2004 Copyright (c) 2002-2005 Delft University of Technology Jaffalaan 5,
 * 2628 BX Delft, the Netherlands. All rights reserved. This software is proprietary information of Delft
 * University of Technology The code is published under the Lesser General Public License
 */
package nl.tudelft.simulation.language.d2;

import java.util.ArrayList;
import java.util.List;

import javax.vecmath.Point2d;

/**
 * DirectionalShape is used to create a shape out of vertices and find out whether a certain point is inside
 * or outside of the shape.
 * <p>
 * (c) copyright 2002-2005 <a href="http://www.simulation.tudelft.nl">Delft University of Technology </a>, the
 * Netherlands.
 * <p>
 * See for project information <a
 * href="http://www.simulation.tudelft.nl/dsol/language">www.simulation.tudelft.nl/language </a> <br>
 * License of use: <a href="http://www.gnu.org/copyleft/lesser.html">Lesser General Public License (LGPL)
 * </a>, no warranty
 * 
 * @version $Revision: 1.8 $ $Date: 2005/08/11 05:49:12 $
 * @author <a href="mailto:royc@tbm.tudelft.nl">Roy Chin </a>
 */
public final class DirectionalShape
{
    /** points that span up the shape. */
    private List<Point2d> points = new ArrayList<Point2d>();

    /** lines that connect the points. */
    private List<DirectionalLine> lines = new ArrayList<DirectionalLine>();

    /** the default last side. */
    public static final int DEFAULT_LAST_SIDE = -10;

    /**
     * constructs a new directional line.
     */
    public DirectionalShape()
    {
        super();
    }

    /**
     * add a point to the shape.
     * 
     * @param x
     *            X coordinate
     * @param y
     *            Y coordinate
     */
    public void addPoint(final double x, final double y)
    {
        this.points.add(new Point2d(x, y));
    }

    /**
     * determine the line segments between the points.
     */
    public void determineSegments()
    {
        // First clear possible previous segments
        this.lines.clear();
        // All segments but the closing segment
        for (int i = 0; i < this.points.size() - 1; i++)
        {
            double x1 = this.points.get(i).x;
            double y1 = this.points.get(i).y;
            double x2 = this.points.get(i + 1).x;
            double y2 = this.points.get(i + 1).y;
            DirectionalLine line = new DirectionalLine(x1, y1, x2, y2);
            this.lines.add(line);
        }
        // The closing segment
        double x1 = this.points.get(this.points.size() - 1).x;
        double y1 = this.points.get(this.points.size() - 1).y;
        double x2 = this.points.get(0).x;
        double y2 = this.points.get(0).y;
        DirectionalLine line = new DirectionalLine(x1, y1, x2, y2);
        this.lines.add(line);
    }

    /**
     * determine whether a point (x,y) is inside this shape or not.
     * 
     * @param x
     *            X coordinate
     * @param y
     *            Y coodinate
     * @return True if (x,y) is inside this shape
     */
    public boolean getInside(final double x, final double y)
    {
        boolean result = true;
        // Note -10 is just an arbritrary number that is not
        // being used as one of the constants in DirectionalLine
        int lastSide = DEFAULT_LAST_SIDE;
        for (DirectionalLine line : this.lines)
        {
            int side = line.getSideThin(x, y);
            if (lastSide != DEFAULT_LAST_SIDE)
            {
                if (side != lastSide)
                {
                    result = false;
                }
            }
            lastSide = side;
        }
        return result;
    }
}