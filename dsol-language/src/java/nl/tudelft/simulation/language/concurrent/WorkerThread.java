/*
 * @(#) SimulatorRunThread.java Oct 15, 2003 Copyright (c) 2002-2005 Delft University of Technology Jaffalaan
 * 5, 2628 BX Delft, the Netherlands. All rights reserved. This software is proprietary information of Delft
 * University of Technology The code is published under the Lesser General Public License
 */
package nl.tudelft.simulation.language.concurrent;

import java.util.logging.Logger;

/**
 * The WorkerThread is a working thread. The thread sleeps while not interrupted. If interuppted the jon.run
 * operation is invoked.
 * <p>
 * (c) copyright 2002-2005 <a href="http://www.simulation.tudelft.nl">Delft University of Technology </a>, the
 * Netherlands.
 * <p>
 * See for project information <a
 * href="http://www.simulation.tudelft.nl/dsol/language">www.simulation.tudelft.nl/language </a> <br>
 * License of use: <a href="http://www.gnu.org/copyleft/lesser.html">Lesser General Public License (LGPL)
 * </a>, no warranty
 * 
 * @version $Revision: 1.10 $ $Date: 2005/08/11 05:49:12 $
 * @author <a href="http://www.peter-jacobs.com/index.htm">Peter Jacobs </a>, <a
 *         href="mailto:a.verbraeck@tbm.tudelft.nl">Alexander Verbraeck </a>
 */
public class WorkerThread extends Thread
{
    /** the job to execute. */
    private Runnable job = null;

    /** finalized. */
    private boolean finalized = false;

    /**
     * constructs a new SimulatorRunThread.
     * 
     * @param name
     *            the name of the thread
     * @param job
     *            the job to run
     */
    public WorkerThread(final String name, final Runnable job)
    {
        super(name);
        this.job = job;
        this.setDaemon(false);
        this.setPriority(Thread.NORM_PRIORITY);
        this.start();
    }

    /**
     * @see java.lang.Object#finalize()
     */
    @Override
    public final synchronized void finalize()
    {
        this.finalized = true;
        try
        {
            super.finalize();
        }
        catch (Throwable exception)
        {
            Logger.getLogger("nl.tudelft.simulation.language.concurrent").warning(exception.getMessage());
            exception.printStackTrace();
        }
    }

    /**
     * @see java.lang.Runnable#run()
     */
    @Override
    public final synchronized void run()
    {
        while (!this.finalized) // always until finalized
        {
            try
            {
                this.wait(); // as long as possible
            }
            catch (InterruptedException interruptedException)
            {
                this.interrupt(); // set the status to interrupted
                try
                {
                    this.job.run();
                }
                catch (Exception exception)
                {
                    Logger.getLogger("nl.tudelft.simulation.language.concurrent").severe(
                            exception.getMessage());
                    exception.printStackTrace();
                }
                Thread.interrupted();
            }
        }
    }
}
