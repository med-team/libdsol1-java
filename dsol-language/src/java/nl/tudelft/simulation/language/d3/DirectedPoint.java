/*
 * @(#) DirectedPoint.java Sep 6, 2003 Copyright (c) 2002-2005 Delft University of Technology Jaffalaan 5,
 * 2628 BX Delft, the Netherlands. All rights reserved. This software is proprietary information of Delft
 * University of Technology The code is published under the Lesser General Public License
 */
package nl.tudelft.simulation.language.d3;

import java.awt.geom.Point2D;

import javax.vecmath.Point3d;
import javax.vecmath.Point3f;
import javax.vecmath.Tuple3d;
import javax.vecmath.Tuple3f;

/**
 * The location object.
 * <p>
 * (c) copyright 2002-2005 <a href="http://www.simulation.tudelft.nl">Delft University of Technology </a>, the
 * Netherlands.
 * <p>
 * See for project information <a
 * href="http://www.simulation.tudelft.nl/dsol/language">www.simulation.tudelft.nl/language </a> <br>
 * License of use: <a href="http://www.gnu.org/copyleft/lesser.html">Lesser General Public License (LGPL)
 * </a>, no warranty
 * 
 * @version $Revision: 1.9 $ $Date: 2005/08/11 05:49:11 $
 * @author <a href="http://www.peter-jacobs.com/index.htm">Peter Jacobs </a>
 */
public class DirectedPoint extends CartesianPoint
{
    /** rotX is the rotX. */
    private double rotX = 0.0;

    /** rotY is the rotY-value. */
    private double rotY = 0.0;

    /** rotZ is the rotZ-value. */
    private double rotZ = 0.0;

    /**
     * constructs a new DirectedPoint.
     */
    public DirectedPoint()
    {
        super();
    }

    /**
     * constructs a new DirectedPoint.
     * 
     * @param x
     *            the x value
     * @param y
     *            the y value
     * @param z
     *            the z value
     */
    public DirectedPoint(final double x, final double y, final double z)
    {
        super(x, y, z);
    }

    /**
     * constructs a new DirectedPoint.
     * 
     * @param x
     *            the x value
     * @param y
     *            the y value
     * @param z
     *            the z value
     * @param rotX
     *            rotX
     * @param rotY
     *            rotY
     * @param rotZ
     *            rotZ
     */
    public DirectedPoint(final double x, final double y, final double z, final double rotX,
            final double rotY, final double rotZ)
    {
        super(x, y, z);
        this.rotX = rotX;
        this.rotY = rotY;
        this.rotZ = rotZ;
    }

    /**
     * constructs a new DirectedPoint.
     * 
     * @param point2D
     *            the point
     * @param rotZ
     *            rotZ
     */
    public DirectedPoint(final Point2D point2D, final double rotZ)
    {
        super(point2D);
        this.rotZ = rotZ;
    }

    /**
     * constructs a new DirectedPoint.
     * 
     * @param xyz
     *            the xyx value
     */
    public DirectedPoint(final double[] xyz)
    {
        super(xyz);
    }

    /**
     * constructs a new DirectedPoint.
     * 
     * @param cartesianPoint
     *            the cartesianPoint
     */
    public DirectedPoint(final Point3d cartesianPoint)
    {
        super(cartesianPoint);
    }

    /**
     * constructs a new DirectedPoint.
     * 
     * @param sphericalPoint
     *            the sphericalPoint
     */
    public DirectedPoint(final SphericalPoint sphericalPoint)
    {
        this(sphericalPoint.toCartesianPoint());
    }

    /**
     * constructs a new DirectedPoint.
     * 
     * @param location
     *            the location
     */
    public DirectedPoint(final DirectedPoint location)
    {
        super(location);
        this.rotY = location.rotY;
        this.rotZ = location.rotZ;
        this.rotX = location.rotX;
    }

    /**
     * constructs a new DirectedPoint.
     * 
     * @param point2D
     *            the point
     */
    public DirectedPoint(final Point2D point2D)
    {
        super(point2D);
    }

    /**
     * constructs a new DirectedPoint.
     * 
     * @param point
     *            the point
     */
    public DirectedPoint(final Point3f point)
    {
        super(point);
    }

    /**
     * constructs a new DirectedPoint.
     * 
     * @param tuple
     *            the point
     */
    public DirectedPoint(final Tuple3d tuple)
    {
        super(tuple);
    }

    /**
     * constructs a new DirectedPoint.
     * 
     * @param tuple
     *            the point
     */
    public DirectedPoint(final Tuple3f tuple)
    {
        super(tuple);
    }

    /**
     * returns ther rotY-value.
     * 
     * @return double
     */
    public double getRotY()
    {
        return this.rotY;
    }

    /**
     * sets the rotY.
     * 
     * @param rotY
     *            the rotY-value
     */
    public void setRotY(final double rotY)
    {
        this.rotY = rotY;
    }

    /**
     * returns the rotZ value.
     * 
     * @return double
     */
    public double getRotZ()
    {
        return this.rotZ;
    }

    /**
     * sets the rotZ value.
     * 
     * @param rotZ
     *            the rotZ-value
     */
    public void setRotZ(final double rotZ)
    {
        this.rotZ = rotZ;
    }

    /**
     * returns the rotX value.
     * 
     * @return double
     */
    public double getRotX()
    {
        return this.rotX;
    }

    /**
     * sets the rotX.
     * 
     * @param rotX
     *            rotX-value
     */
    public void setRotX(final double rotX)
    {
        this.rotX = rotX;
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public final String toString()
    {
        return "[position=" + super.toString() + ";RotX=" + this.rotX + ";RotY=" + this.rotY + ";RotZ="
                + this.rotZ + "]";
    }

    /**
     * @see java.lang.Object#clone()
     */
    @Override
    public Object clone()
    {
        return new DirectedPoint(this.x, this.y, this.z, this.rotX, this.rotY, this.rotZ);
    }

    /**
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object arg0)
    {
        if (!(arg0 instanceof DirectedPoint))
        {
            return false;
        }
        DirectedPoint loc = (DirectedPoint) arg0;
        return (super.equals(arg0) && loc.rotX == this.rotX && loc.rotY == this.rotY && loc.rotZ == this.rotZ);
    }

    /**
     * @see javax.vecmath.Tuple3d#equals(javax.vecmath.Tuple3d)
     */
    @Override
    public boolean equals(final Tuple3d arg0)
    {
        return this.equals((Object) arg0);
    }

    /**
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        return super.hashCode();
    }
}
