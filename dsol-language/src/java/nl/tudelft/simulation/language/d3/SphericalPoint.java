/*
 * @(#) SphericalPoint.java Aug 4, 2004 Copyright (c) 2002-2005 Delft University of Technology Jaffalaan 5,
 * 2628 BX Delft, the Netherlands. All rights reserved. This software is proprietary information of Delft
 * University of Technology The code is published under the Lesser General Public License
 */
package nl.tudelft.simulation.language.d3;

/**
 * A sphericalpoint as defined in <a href="http://mathworld.wolfram.com/SphericalCoordinates.html">
 * http://mathworld.wolfram.com/SphericalCoordinates.html </a>.
 * <p>
 * (c) copyright 2002-2005 <a href="http://www.simulation.tudelft.nl">Delft University of Technology </a>, the
 * Netherlands.
 * <p>
 * See for project information <a
 * href="http://www.simulation.tudelft.nl/dsol/language">www.simulation.tudelft.nl/language </a> <br>
 * License of use: <a href="http://www.gnu.org/copyleft/lesser.html">Lesser General Public License (LGPL)
 * </a>, no warranty
 * 
 * @author <a href="http://www.peter-jacobs.com/index.htm">Peter Jacobs </a>
 * @version $Revision: 1.8 $ $Date: 2005/08/11 05:49:11 $
 * @since 1.5
 */
public class SphericalPoint
{
    /** radius. */
    private double radius = 0.0;

    /** phi. */
    private double phi = 0.0;

    /** theta. */
    private double theta = 0.0;

    /**
     * constructs a new SphericalPoint.
     * 
     * @param phi
     *            phi
     * @param radius
     *            radius
     * @param theta
     *            theta
     */
    public SphericalPoint(final double radius, final double phi, final double theta)
    {
        super();
        this.phi = phi;
        this.radius = radius;
        this.theta = theta;
    }

    /**
     * @return phi
     */
    public double getPhi()
    {
        return this.phi;
    }

    /**
     * @return radius
     */
    public double getRadius()
    {
        return this.radius;
    }

    /**
     * @return theta
     */
    public double getTheta()
    {
        return this.theta;
    }

    /**
     * converts a sphericalpoint to a cartesian point.
     * 
     * @return the cartesian point
     */
    public CartesianPoint toCartesianPoint()
    {
        return SphericalPoint.toCartesianPoint(this);
    }

    /**
     * converts a sphericalpoint to a cartesian point.
     * 
     * @param point
     *            the sphericalpoint
     * @return the cartesian point
     */
    public static CartesianPoint toCartesianPoint(final SphericalPoint point)
    {
        double x = point.radius * Math.sin(point.phi) * Math.cos(point.theta);
        double y = point.radius * Math.sin(point.phi) * Math.sin(point.theta);
        double z = point.radius * Math.cos(point.phi);
        return new CartesianPoint(x, y, z);
    }
}
