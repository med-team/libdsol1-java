/*
 * @(#) BoundsUtil.java Mar 3, 2004 Copyright (c) 2002-2005 Delft University of Technology Jaffalaan 5, 2628
 * BX Delft, the Netherlands. All rights reserved. This software is proprietary information of Delft
 * University of Technology The code is published under the Lesser General Public License
 */

package nl.tudelft.simulation.language.d3;

import java.awt.geom.Rectangle2D;

import javax.media.j3d.Bounds;
import javax.media.j3d.Transform3D;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

/**
 * A BoundsUtil.
 * <p>
 * (c) copyright 2002-2005 <a href="http://www.simulation.tudelft.nl">Delft University of Technology </a>, the
 * Netherlands.
 * <p>
 * See for project information <a
 * href="http://www.simulation.tudelft.nl/dsol/language">www.simulation.tudelft.nl/language </a> <br>
 * License of use: <a href="http://www.gnu.org/copyleft/lesser.html">Lesser General Public License (LGPL)
 * </a>, no warranty
 * 
 * @version $Revision: 1.9 $ $Date: 2005/08/11 05:49:11 $
 * @author <a href="http://www.peter-jacobs.com">Peter Jacobs </a>
 */
public final class BoundsUtil
{
    /**
     * constructs a new BoundsUtil.
     */
    private BoundsUtil()
    {
        super();
        // unreachable code
    }

    /**
     * computes the intersect of bounds with the zValue.
     * 
     * @param bounds
     *            the bounds
     * @param center
     *            the
     * @param zValue
     *            the zValue
     * @return Rectangle2D the result
     */
    public static Rectangle2D getIntersect(final DirectedPoint center, final Bounds bounds,
            final double zValue)
    {
        BoundingBox box = new BoundingBox((Bounds) bounds.clone());
        Transform3D transform = new Transform3D();
        transform.rotZ(center.getRotZ());
        transform.rotY(center.getRotY());
        transform.rotX(center.getRotX());
        transform.setTranslation(new Vector3d(new Point3d(center.x, center.y, center.z)));
        box.transform(transform);

        Point3d lower = new Point3d();
        box.getLower(lower);
        lower.set(lower.x, lower.y, zValue);
        if (!box.intersect(lower))
        {
            return null;
        }
        Point3d upper = new Point3d();
        box.getUpper(upper);
        return new Rectangle2D.Double(lower.x, lower.y, (upper.x - lower.x), (upper.y - lower.y));
    }

    /**
     * rotates and translates to a directed point.
     * 
     * @param point
     *            the point
     * @param bounds
     *            the bounds
     * @return the bounds
     */
    public static Bounds transform(final Bounds bounds, final DirectedPoint point)
    {
        Bounds result = (Bounds) bounds.clone();

        // First we rotate around 0,0,0
        Transform3D transform = new Transform3D();
        transform.rotX(point.getRotX());
        transform.rotY(point.getRotY());
        transform.rotZ(point.getRotZ());
        transform.setTranslation(new Vector3d(point));
        result.transform(transform);
        return result;
    }

    /**
     * @param center
     *            the center of the bounds
     * @param bounds
     *            the bounds
     * @param point
     *            the point
     * @return whether or not the point is in the bounds
     */
    public static boolean contains(final DirectedPoint center, final Bounds bounds, final Point3d point)
    {
        BoundingBox box = new BoundingBox((Bounds) bounds.clone());
        Transform3D transform = new Transform3D();
        transform.rotZ(center.getRotZ());
        transform.rotY(center.getRotY());
        transform.rotX(center.getRotX());
        transform.setTranslation(new Vector3d(center));
        box.transform(transform);
        Point3d lower = new Point3d();
        box.getLower(lower);
        Point3d upper = new Point3d();
        box.getUpper(upper);
        return (point.x >= lower.x && point.x <= upper.x && point.y >= lower.y && point.y <= upper.y
                && point.z >= lower.z && point.z <= upper.z);
    }
}
