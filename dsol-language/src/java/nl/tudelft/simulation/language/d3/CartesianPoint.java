/*
 * @(#) CartesianPoint.java Nov 12, 2003 Copyright (c) 2002-2005 Delft University of Technology Jaffalaan 5,
 * 2628 BX Delft, the Netherlands. All rights reserved. This software is proprietary information of Delft
 * University of Technology The code is published under the Lesser General Public License
 */
package nl.tudelft.simulation.language.d3;

import java.awt.geom.Point2D;

import javax.vecmath.Point3f;
import javax.vecmath.Tuple3d;
import javax.vecmath.Tuple3f;

/**
 * The Point3D class with utilities to convert to point2D where the z-axis is neglected.
 * <p>
 * (c) copyright 2002-2005 <a href="http://www.simulation.tudelft.nl">Delft University of Technology </a>, the
 * Netherlands.
 * <p>
 * See for project information <a
 * href="http://www.simulation.tudelft.nl/dsol/language">www.simulation.tudelft.nl/language </a> <br>
 * License of use: <a href="http://www.gnu.org/copyleft/lesser.html">Lesser General Public License (LGPL)
 * </a>, no warranty
 * 
 * @version $Revision: 1.8 $ $Date: 2005/08/11 05:49:11 $
 * @author <a href="http://www.peter-jacobs.com/index.htm">Peter Jacobs </a>
 */
public class CartesianPoint extends javax.vecmath.Point3d
{
    /**
     * constructs a new CartesianPoint.
     * 
     * @param x
     *            x
     * @param y
     *            y
     * @param z
     *            z
     */
    public CartesianPoint(final double x, final double y, final double z)
    {
        super(x, y, z);
    }

    /**
     * constructs a new CartesianPoint.
     * 
     * @param xyz
     *            x,y,z
     */
    public CartesianPoint(final double[] xyz)
    {
        super(xyz);
    }

    /**
     * constructs a new CartesianPoint.
     * 
     * @param point
     *            point3d
     */
    public CartesianPoint(final javax.vecmath.Point3d point)
    {
        super(point);
    }

    /**
     * constructs a new CartesianPoint.
     * 
     * @param point
     *            point3d
     */
    public CartesianPoint(final Point3f point)
    {
        super(point);
    }

    /**
     * constructs a new CartesianPoint.
     * 
     * @param tuple
     *            tuple
     */
    public CartesianPoint(final Tuple3f tuple)
    {
        super(tuple);
    }

    /**
     * constructs a new CartesianPoint.
     * 
     * @param tuple
     *            point3d
     */
    public CartesianPoint(final Tuple3d tuple)
    {
        super(tuple);
    }

    /**
     * constructs a new CartesianPoint.
     * 
     * @param point2D
     *            a 2D point
     */
    public CartesianPoint(final Point2D point2D)
    {
        this(point2D.getX(), point2D.getY(), 0);
    }

    /**
     * constructs a new CartesianPoint.
     */
    public CartesianPoint()
    {
        super();
    }

    /**
     * returns the 2D representation of the point.
     * 
     * @return Point2D the result
     */
    public final Point2D to2D()
    {
        return new Point2D.Double(this.x, this.y);
    }

    /**
     * converts the point to a sperical point.
     * 
     * @return the spherical point
     */
    public final SphericalPoint toCartesianPoint()
    {
        return CartesianPoint.toSphericalPoint(this);
    }

    /**
     * converts a cartesian point to a sperical point.
     * 
     * @param point
     *            the cartesian point
     * @return the spherical point
     */
    public static SphericalPoint toSphericalPoint(final CartesianPoint point)
    {
        double rho = Math.sqrt(Math.pow(point.x, 2) + Math.pow(point.y, 2) + Math.pow(point.z, 2));
        double s = Math.sqrt(Math.pow(point.x, 2) + Math.pow(point.y, 2));
        double phi = Math.acos(point.z / rho);
        double theta = Math.asin(point.y / s);
        if (point.x >= 0)
        {
            theta = Math.PI - theta;
        }
        return new SphericalPoint(phi, rho, theta);
    }
}
