/*
 * @(#) Filterinterface.java Oct 26, 2004 Copyright (c) 2002-2005 Delft
 * University of Technology Jaffalaan 5, 2628 BX Delft, the Netherlands. All
 * rights reserved. This software is proprietary information of Delft University
 * of Technology The code is published under the Lesser General Public License
 */
package nl.tudelft.simulation.language.filters;

import java.io.Serializable;

/**
 * The FilterInterface is a general interface for all filters in DSOL. Filters
 * can be based on xY combinations, class information ,etc. etc. The API of
 * implementing filters will explain what it expects as input.
 * <p>
 * (c) copyright 2002-2005 <a href="http://www.simulation.tudelft.nl">Delft
 * University of Technology </a>, the Netherlands.
 * <p>
 * See for project information <a
 * href="http://www.simulation.tudelft.nl/dsol/language">www.simulation.tudelft.nl/language
 * </a> <br>
 * License of use: <a href="http://www.gnu.org/copyleft/lesser.html">Lesser
 * General Public License (LGPL) </a>, no warranty
 * 
 * @author <a
 *         href="http://web.eur.nl/fbk/dep/dep1/Introduction/Staff/People/Lang">Niels
 *         Lang </a><a href="http://www.peter-jacobs.com/index.htm">Peter
 *         Jacobs </a>
 * @version $Revision: 1.10 $ $Date: 2005/07/04 12:21:25 $
 * @since 1.5
 */
public interface Filterinterface extends Serializable
{
    /**
     * a filter defines whether to accept a value in a chart
     * 
     * @param entry the entry to filter
     * @return whether to accept this entry
     */
    boolean accept(Object entry);

    /**
     * inverts the filter
     * 
     * @param inverted whether to invert the filter
     */
    void setInverted(boolean inverted);

    /**
     * is the filter inverted?
     * 
     * @return whether the filter is inverted.
     */
    boolean isInverted();

    /**
     * returns a string representation of the criterium
     * 
     * @return the string representing the criterium
     */
    String getCriterium();

    /**
     * adds filter to this filter and returns the composed filter
     * 
     * @param filter the filter to add
     * @return the composed filter
     */
    Filterinterface and(Filterinterface filter);

    /**
     * creates a new composite filter which is one or two
     * 
     * @param filter the filter to add
     * @return the composed filter
     */
    Filterinterface or(Filterinterface filter);
}