/*
 * @(#) AbstractFilter.java Oct 26, 2004 Copyright (c) 2002-2005 Delft
 * University of Technology Jaffalaan 5, 2628 BX Delft, the Netherlands. All
 * rights reserved. This software is proprietary information of Delft University
 * of Technology The code is published under the Lesser General Public License
 */
package nl.tudelft.simulation.language.filters;

/**
 * The abstract filter forms the abstract class for all filters. The filter
 * method should be implemented by all subclasses. This filter method should
 * have the same semantics as the accept(inverted=false) method.
 * <p>
 * (c) copyright 2002-2005 <a href="http://www.simulation.tudelft.nl">Delft
 * University of Technology </a>, the Netherlands.
 * <p>
 * See for project information <a
 * href="http://www.simulation.tudelft.nl/dsol/language">www.simulation.tudelft.nl/language
 * </a> <br>
 * License of use: <a href="http://www.gnu.org/copyleft/lesser.html">Lesser
 * General Public License (LGPL) </a>, no warranty
 * 
 * @author <a
 *         href="http://web.eur.nl/fbk/dep/dep1/Introduction/Staff/People/Lang">Niels
 *         Lang </a><a href="http://www.peter-jacobs.com/index.htm">Peter
 *         Jacobs </a>
 * @version $Revision: 1.10 $ $Date: 2005/08/04 12:08:54 $
 * @since 1.5
 */
public abstract class AbstractFilter implements Filterinterface
{
    /** is this filter inverted */
    protected boolean inverted = false;

    /**
     * constructs a new AbstractFilter
     */
    public AbstractFilter()
    {
        super();
    }

    /**
     * @see nl.tudelft.simulation.language.filters.Filterinterface#isInverted()
     */
    public boolean isInverted()
    {
        return this.inverted;
    }

    /**
     * @see nl.tudelft.simulation.language.filters.Filterinterface
     *      #setInverted(boolean)
     */
    public void setInverted(final boolean inverted)
    {
        this.inverted = inverted;
    }

    /**
     * @see nl.tudelft.simulation.language.filters.Filterinterface#accept(java.lang.Object)
     */
    public boolean accept(final Object entry)
    {
        boolean value = this.filter(entry);
        if (!this.inverted)
        {
            return value;
        }
        return !value;
    }

    /**
     * filters the entry. This method should be implemented by every filter
     * based on its semantic meaning.
     * 
     * @param entry the entry to filter.
     * @return whether to accept the value.
     */
    protected abstract boolean filter(final Object entry);

    /**
     * returns the filter criterium
     * 
     * @return the criterium
     */
    public abstract String getCriterium();

    /**
     * adds filter to this filter and returns the composed filter
     * 
     * @param filter the filter to add
     * @return the composed filter
     */
    public Filterinterface and(final Filterinterface filter)
    {
        return new CompositeFilter(this, filter, CompositeFilter.AND);
    }

    /**
     * creates a new composite filter which is one or two
     * 
     * @param filter the filter to add
     * @return the composed filter
     */
    public Filterinterface or(final Filterinterface filter)
    {
        return new CompositeFilter(this, filter, CompositeFilter.OR);
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
	public String toString()
    {
        return "Filter[criterium=" + this.getCriterium() + ";inverted="
                + this.inverted + "]";
    }

}