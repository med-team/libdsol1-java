/*
 * @(#) MaxPointFilter.java Oct 26, 2004 Copyright (c) 2002-2005 Delft University of Technology Jaffalaan 5,
 * 2628 BX Delft, the Netherlands. All rights reserved. This software is proprietary information of Delft
 * University of Technology The code is published under the Lesser General Public License
 */
package nl.tudelft.simulation.language.filters;

/**
 * The histogram specifies a histogram chart for the DSOL framework.
 * <p>
 * (c) copyright 2002-2005 <a href="http://www.simulation.tudelft.nl">Delft University of Technology </a>, the
 * Netherlands.
 * <p>
 * See for project information <a
 * href="http://www.simulation.tudelft.nl/dsol/language">www.simulation.tudelft.nl/language </a> <br>
 * License of use: <a href="http://www.gnu.org/copyleft/lesser.html">Lesser General Public License (LGPL)
 * </a>, no warranty
 * 
 * @author <a href="http://www.peter-jacobs.com/index.htm"> Peter Jacobs </a>
 * @version $Revision: 1.9 $ $Date: 2005/08/11 05:49:12 $
 * @since 1.5
 */
public class MaxPointFilter extends AbstractFilter
{
    /** the maxPoints to use. */
    private long maxPoints = -1;

    /** the amount of points already accepted. */
    private long accepted = 0;

    /**
     * constructs a new MaxPointFilter.
     * 
     * @param maxPoints
     *            the maximum points to display
     */
    public MaxPointFilter(final long maxPoints)
    {
        super();
        this.maxPoints = maxPoints;
    }

    /**
     * @see nl.tudelft.simulation.language.filters.AbstractFilter#filter(java.lang.Object)
     */
    @Override
    protected synchronized boolean filter(final Object entry)
    {
        this.accepted++;
        if (this.accepted > this.maxPoints)
        {
            return false;
        }
        return true;
    }

    /**
     * @see nl.tudelft.simulation.language.filters.Filterinterface#getCriterium()
     */
    @Override
    public String getCriterium()
    {
        return "accepts the first MaxPoint(=" + this.maxPoints + ") entries";
    }
}
