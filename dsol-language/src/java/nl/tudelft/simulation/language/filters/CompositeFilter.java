/*
 * @(#) CompositeFilter.java Oct 26, 2004 Copyright (c) 2002-2005 Delft
 * University of Technology Jaffalaan 5, 2628 BX Delft, the Netherlands. All
 * rights reserved. This software is proprietary information of Delft University
 * of Technology The code is published under the Lesser General Public License
 */
package nl.tudelft.simulation.language.filters;

/**
 * The composite filter combines two filters.
 * <p>
 * (c) copyright 2002-2005 <a href="http://www.simulation.tudelft.nl">Delft
 * University of Technology </a>, the Netherlands.
 * <p>
 * See for project information <a
 * href="http://www.simulation.tudelft.nl/dsol/language">www.simulation.tudelft.nl/language
 * </a> <br>
 * License of use: <a href="http://www.gnu.org/copyleft/lesser.html">Lesser
 * General Public License (LGPL) </a>, no warranty
 * 
 * @author <a
 *         href="http://web.eur.nl/fbk/dep/dep1/Introduction/Staff/People/Lang">Niels
 *         Lang </a><a href="http://www.peter-jacobs.com/index.htm">Peter
 *         Jacobs </a>
 * @version $Revision: 1.12 $ $Date: 2005/08/04 12:08:54 $
 * @since 1.5
 */
public class CompositeFilter extends AbstractFilter
{
    /** the AND operator */
    public static final short AND = 0;

    /** the OR operator */
    public static final short OR = 1;

    /** the operator of the composite filter */
    private short operator = -1;

    /** the filters to compose */
    private Filterinterface[] filters = new Filterinterface[2];

    /**
     * constructs a new CompositeFilter
     * 
     * @param filter1 the first filter
     * @param filter2 the second filter
     * @param operator the operator (AND or OR)
     */
    public CompositeFilter(final Filterinterface filter1,
            final Filterinterface filter2, final short operator)
    {
        super();
        if (operator < 0 || operator > 1)
        {
            throw new IllegalArgumentException("unknown operator");
        }
        this.filters[0] = filter1;
        this.filters[1] = filter2;
        this.operator = operator;
    }

    /**
     * @see nl.tudelft.simulation.language.filters.AbstractFilter#filter(java.lang.Object)
     */
    @Override
	protected boolean filter(final Object entry)
    {
        if (this.operator == CompositeFilter.AND)
        {
            return this.filters[0].accept(entry)
                    && this.filters[1].accept(entry);
        }
        return this.filters[0].accept(entry) || this.filters[1].accept(entry);
    }

    /**
     * Converts the operator of this filter into a human readable string.
     * 
     * @return the operator in human readable string
     */
    protected String operatorToString()
    {
        if (this.operator == AND)
        {
            return "AND";
        }
        return "OR";
    }

    /**
     * @see nl.tudelft.simulation.language.filters.AbstractFilter#getCriterium()
     */
    @Override
	public String getCriterium()
    {
        return "composed[" + this.filters[0].getCriterium() + ";"
                + operatorToString() + ";" + this.filters[1].getCriterium()
                + "]";
    }
}
