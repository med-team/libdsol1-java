/*
 * @(#) SerializableConstructor.java Jan 20, 2004 Copyright (c) 2002-2005 Delft University of Technology
 * Jaffalaan 5, 2628 BX Delft, the Netherlands. All rights reserved. This software is proprietary information
 * of Delft University of Technology The code is published under the Lesser General Public License
 */
package nl.tudelft.simulation.language.reflection;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.Constructor;

/**
 * A SerializableConstructor.
 * <p>
 * (c) copyright 2002-2005 <a href="http://www.simulation.tudelft.nl">Delft University of Technology </a>, the
 * Netherlands.
 * <p>
 * See for project information <a
 * href="http://www.simulation.tudelft.nl/dsol/language">www.simulation.tudelft.nl/language </a> <br>
 * License of use: <a href="http://www.gnu.org/copyleft/lesser.html">Lesser General Public License (LGPL)
 * </a>, no warranty
 * 
 * @author <a href="http://www.peter-jacobs.com/index.htm">Peter Jacobs </a>
 * @version $Revision: 1.8 $ $Date: 2005/08/11 05:49:11 $
 * @since 1.5
 */
public class SerializableConstructor implements Serializable
{
    /** the constructor */
    private Constructor<?> constructor = null;

    /**
     * constructs a new SerializableConstructor
     * 
     * @param constructor
     *            The constructor
     */
    public SerializableConstructor(final Constructor<?> constructor)
    {
        super();
        this.constructor = constructor;
    }

    /**
     * constructs a new SerializableConstructor
     * 
     * @param clazz
     *            the clazz this field is instance of
     * @param parameterTypes
     *            the parameterTypes of the constructor
     * @throws NoSuchMethodException
     *             whenever the method is not defined in clazz
     */
    @SuppressWarnings("unchecked")
    public SerializableConstructor(final Class<?> clazz, final Class[] parameterTypes)
            throws NoSuchMethodException
    {
        this.constructor = ClassUtil.resolveConstructor(clazz, parameterTypes);
    }

    /**
     * deserializes the field
     * 
     * @return the Constructor
     */
    public Constructor<?> deSerialize()
    {
        return this.constructor;
    }

    /**
     * writes a serializable method to stream
     * 
     * @param out
     *            the outputstream
     * @throws IOException
     *             on IOException
     */
    private void writeObject(final ObjectOutputStream out) throws IOException
    {
        try
        {
            out.writeObject(this.constructor.getDeclaringClass());
            out.writeObject(new MethodSignature(this.constructor));
        }
        catch (Exception exception)
        {
            throw new IOException(exception.getMessage());
        }
    }

    /**
     * reads a serializable method from stream
     * 
     * @param in
     *            the inputstream
     * @throws IOException
     *             on IOException
     */
    private void readObject(final java.io.ObjectInputStream in) throws IOException
    {
        try
        {
            Class<?> declaringClass = (Class) in.readObject();
            MethodSignature signature = (MethodSignature) in.readObject();
            this.constructor = ClassUtil.resolveConstructor(declaringClass, signature.getParameterTypes());
        }
        catch (Exception exception)
        {
            throw new IOException(exception.getMessage());
        }
    }
}
