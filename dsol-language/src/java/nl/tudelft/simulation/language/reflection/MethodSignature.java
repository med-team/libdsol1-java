/*
 * @(#) MethodSignature.java Jan 12, 2004 Copyright (c) 2002-2005 Delft
 * University of Technology Jaffalaan 5, 2628 BX Delft, the Netherlands. All
 * rights reserved. This software is proprietary information of Delft University
 * of Technology The code is published under the Lesser General Public License
 */
package nl.tudelft.simulation.language.reflection;

import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * A method descriptor represents the parameters that the method takes and the
 * value that it returns. It is a series of characters generated by the grammar
 * described at <a href =
 * "http://java.sun.com/docs/books/vmspec/2nd-edition/html/ClassFile.doc.html#1169">
 * The Java Virtual Machine Specification </a>.
 * <p>
 * (c) copyright 2002-2005 <a href="http://www.simulation.tudelft.nl">Delft
 * University of Technology </a>, the Netherlands.
 * <p>
 * See for project information <a
 * href="http://www.simulation.tudelft.nl/dsol/language">www.simulation.tudelft.nl/language
 * </a> <br>
 * License of use: <a href="http://www.gnu.org/copyleft/lesser.html">Lesser
 * General Public License (LGPL) </a>, no warranty
 * 
 * @author <a href="http://www.peter-jacobs.com/index.htm">Peter Jacobs </a>, <a
 *         href="mailto:nlang@fbk.eur.nl">Niels Lang </a><a
 *         href="mailto:a.verbraeck@tbm.tudelft.nl">Alexander
 *         Verbraeck </a>
 * @version $Revision: 1.9 $ $Date: 2005/08/11 05:49:11 $
 * @since 1.5
 */
public class MethodSignature implements Serializable
{
    /** the value of the methodDescriptor */
    private String value = null;

    /**
     * constructs a new MethodSignature
     * 
     * @param value the descriptor
     */
    public MethodSignature(final String value)
    {
        super();
        this.value = value;
    }

    /**
     * constructs a new MethodSignature
     * 
     * @param method the method
     */
    public MethodSignature(final Method method)
    {
        super();
        Class[] parameterTypes = new Class[0];
        if (method.getParameterTypes() != null)
        {
            parameterTypes = method.getParameterTypes();
        }
        this.value = "(";
        for (int i = 0; i < parameterTypes.length; i++)
        {
            this.value = this.value
                    + FieldSignature.toDescriptor(parameterTypes[i]);
        }
        this.value = this.value + ")"
                + FieldSignature.toDescriptor(method.getReturnType());
    }

    /**
     * constructs a new MethodSignature
     * 
     * @param constructor the constructor
     */
    public MethodSignature(final Constructor< ? > constructor)
    {
        super();
        Class[] parameterTypes = new Class[0];
        if (constructor.getParameterTypes() != null)
        {
            parameterTypes = constructor.getParameterTypes();
        }

        this.value = "(";
        for (int i = 0; i < parameterTypes.length; i++)
        {
            this.value = this.value
                    + FieldSignature.toDescriptor(parameterTypes[i]);
        }
        this.value = this.value + ")"
                + FieldSignature.toDescriptor(constructor.getDeclaringClass());
    }

    /**
     * @return Returns the parameterDescriptor
     */
    public String getParameterDescriptor()
    {
        return MethodSignature.getParameterDescriptor(this.value);
    }

    /**
     * returns the parameterTypes
     * 
     * @return ClassDescriptor[] the result
     * @throws ClassNotFoundException on incomplete classPath
     */
    public Class[] getParameterTypes() throws ClassNotFoundException
    {
        return MethodSignature.getParameterTypes(this.value);
    }

    /**
     * @return Returns the returnDescriptor
     */
    public String getReturnDescriptor()
    {
        return MethodSignature.getReturnDescriptor(this.value);
    }

    /**
     * returns the returnType of this methodDescriptor
     * 
     * @return Returns the returnType
     * @throws ClassNotFoundException on incomplete classPath
     */
    public Class getReturnType() throws ClassNotFoundException
    {
        return MethodSignature.getReturnType(this.value);
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
	public String toString()
    {
        return this.value;
    }

    /**
     * @return Returns the parameterDescriptor
     * @param methodDescriptor the methodDescriptor
     */
    public static String getParameterDescriptor(final String methodDescriptor)
    {
        return methodDescriptor.substring(1, methodDescriptor.indexOf(')'));
    }

    /**
     * returns the parameterTypes
     * 
     * @param methodDescriptor the string
     * @return ClassDescriptor[] the result
     * @throws ClassNotFoundException on incomplete classPath
     */
    public static Class[] getParameterTypes(final String methodDescriptor)
            throws ClassNotFoundException
    {
        String parameterDescriptor = MethodSignature
                .getParameterDescriptor(methodDescriptor);
        List<Class> result = new ArrayList<Class>();
        int length = 0;
        while (length < parameterDescriptor.length())
        {
            String array = "";
            while (parameterDescriptor.charAt(length) == '[')
            {
                array = array + "[";
                length++;
            }
            if (parameterDescriptor.charAt(length) == 'L')
            {
                String argument = parameterDescriptor.substring(length);
                argument = array
                        + argument.substring(0, argument.indexOf(';') + 1);
                result.add(FieldSignature.toClass(argument));
                length = length + argument.length() - array.length();
            } else
            {
                result.add(FieldSignature.toClass(array
                        + parameterDescriptor.charAt(length)));
                length++;
            }
        }
        return result.toArray(new Class[result.size()]);
    }

    /**
     * @return Returns the returnDescriptor
     * @param methodDescriptor the methodDescriptor
     */
    public static String getReturnDescriptor(final String methodDescriptor)
    {
        return methodDescriptor.substring(methodDescriptor.indexOf(')') + 1);
    }

    /**
     * returns the returnType of this methodDescriptor
     * 
     * @param methodDescriptor the returnDescriptor
     * @return Returns the returnType
     * @throws ClassNotFoundException on incomplete classPath
     */
    public static Class getReturnType(final String methodDescriptor)
            throws ClassNotFoundException
    {
        return FieldSignature.toClass(MethodSignature
                .getReturnDescriptor(methodDescriptor));
    }
}