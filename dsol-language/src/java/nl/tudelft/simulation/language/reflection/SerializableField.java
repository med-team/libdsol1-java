/*
 * @(#) SerializableField.java Jan 20, 2004 Copyright (c) 2002-2005 Delft University of Technology Jaffalaan
 * 5, 2628 BX Delft, the Netherlands. All rights reserved. This software is proprietary information of Delft
 * University of Technology The code is published under the Lesser General Public License
 */
package nl.tudelft.simulation.language.reflection;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.Field;

/**
 * A SerializableField.
 * <p>
 * (c) copyright 2002-2005 <a href="http://www.simulation.tudelft.nl">Delft University of Technology </a>, the
 * Netherlands.
 * <p>
 * See for project information <a
 * href="http://www.simulation.tudelft.nl/dsol/language">www.simulation.tudelft.nl/language </a> <br>
 * License of use: <a href="http://www.gnu.org/copyleft/lesser.html">Lesser General Public License (LGPL)
 * </a>, no warranty
 * 
 * @author <a href="http://www.peter-jacobs.com/index.htm">Peter Jacobs </a>
 * @version $Revision: 1.8 $ $Date: 2005/08/11 05:49:11 $
 * @since 1.5
 */
public class SerializableField implements Serializable
{
    /** the field */
    private Field field = null;

    /**
     * constructs a new SerializableField
     * 
     * @param field
     *            The field
     */
    public SerializableField(final Field field)
    {
        super();
        this.field = field;
    }

    /**
     * constructs a new SerializableField
     * 
     * @param clazz
     *            the clazz this field is instance of
     * @param fieldName
     *            the name of the field
     * @throws NoSuchFieldException
     *             whenever the field is not defined in clazz
     */
    public SerializableField(final Class clazz, final String fieldName) throws NoSuchFieldException
    {
        this.field = ClassUtil.resolveField(clazz, fieldName);
    }

    /**
     * deserializes the field
     * 
     * @return the Field
     */
    public Field deSerialize()
    {
        return this.field;
    }

    /**
     * writes a serializable method to stream
     * 
     * @param out
     *            the outputstream
     * @throws IOException
     *             on IOException
     */
    private void writeObject(final ObjectOutputStream out) throws IOException
    {
        try
        {
            out.writeObject(this.field.getDeclaringClass());
            out.writeObject(new FieldSignature(this.field.getName()));
        }
        catch (Exception exception)
        {
            throw new IOException(exception.getMessage());
        }
    }

    /**
     * reads a serializable method from stream
     * 
     * @param in
     *            the inputstream
     * @throws IOException
     *             on IOException
     */
    private void readObject(final java.io.ObjectInputStream in) throws IOException
    {
        try
        {
            Class declaringClass = (Class) in.readObject();
            FieldSignature signature = (FieldSignature) in.readObject();
            this.field = ClassUtil.resolveField(declaringClass, signature.getStringValue());
        }
        catch (Exception exception)
        {
            throw new IOException(exception.getMessage());
        }
    }
}
