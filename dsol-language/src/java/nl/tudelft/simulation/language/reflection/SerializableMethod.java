/*
 * @(#) SerializableMethod.java Jan 20, 2004 Copyright (c) 2002-2005 Delft
 * University of Technology Jaffalaan 5, 2628 BX Delft, the Netherlands. All
 * rights reserved. This software is proprietary information of Delft University
 * of Technology The code is published under the Lesser General Public License
 */
package nl.tudelft.simulation.language.reflection;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.Method;

/**
 * A SerializableMethod.
 * <p>
 * (c) copyright 2002-2005 <a href="http://www.simulation.tudelft.nl">Delft
 * University of Technology </a>, the Netherlands.
 * <p>
 * See for project information <a
 * href="http://www.simulation.tudelft.nl/dsol/language">www.simulation.tudelft.nl/language
 * </a> <br>
 * License of use: <a href="http://www.gnu.org/copyleft/lesser.html">Lesser
 * General Public License (LGPL) </a>, no warranty
 * 
 * @author <a href="http://www.peter-jacobs.com/index.htm">Peter Jacobs </a>
 * @version $Revision: 1.7 $ $Date: 2005/07/04 12:21:23 $
 * @since 1.5
 */
public class SerializableMethod implements Serializable
{
    /** the method to use */
    private Method method = null;

    /**
     * constructs a new SerializableMethod
     * 
     * @param method the method
     */
    public SerializableMethod(final Method method)
    {
        super();
        this.method = method;
    }

    /**
     * constructs a new SerializableMethod
     * 
     * @param clazz the clazz this field is instance of
     * @param methodName the name of the method
     * @param parameterTypes The parameterTypes of the method
     * @throws NoSuchMethodException whenever the method is not defined in clazz
     */
    public SerializableMethod(final Class clazz, final String methodName,
            final Class[] parameterTypes) throws NoSuchMethodException
    {
        this.method = ClassUtil
                .resolveMethod(clazz, methodName, parameterTypes);
    }

    /**
     * deserializes the field
     * 
     * @return the Field
     */
    public Method deSerialize()
    {
        return this.method;
    }

    /**
     * writes a serializable method to stream
     * 
     * @param out the outputstream
     * @throws IOException on IOException
     */
    private void writeObject(final ObjectOutputStream out) throws IOException
    {
        try
        {
            out.writeObject(this.method.getDeclaringClass());
            out.writeObject(this.method.getName());
            out.writeObject(new MethodSignature(this.method));
        } catch (Exception exception)
        {
            throw new IOException(exception.getMessage());
        }
    }

    /**
     * reads a serializable method from stream
     * 
     * @param in the inputstream
     * @throws IOException on IOException
     */
    private void readObject(final java.io.ObjectInputStream in)
            throws IOException
    {
        try
        {
            Class declaringClass = (Class) in.readObject();
            String methodName = (String) in.readObject();
            MethodSignature signature = (MethodSignature) in.readObject();
            this.method = ClassUtil.resolveMethod(declaringClass, methodName,
                    signature.getParameterTypes());
        } catch (Exception exception)
        {
            throw new IOException(exception.getMessage());
        }
    }
}