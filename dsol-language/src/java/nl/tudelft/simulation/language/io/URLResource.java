/*
 * @(#) URLResource.java Jun 17, 2004 Copyright (c) 2002-2005 Delft University of Technology Jaffalaan 5, 2628
 * BX Delft, the Netherlands. All rights reserved. This software is proprietary information of Delft
 * University of Technology The code is published under the Lesser General Public License
 */
package nl.tudelft.simulation.language.io;

import java.io.File;
import java.io.InputStream;
import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.net.URL;

/**
 * <p>
 * (c) copyright 2002-2005 <a href="http://www.simulation.tudelft.nl">Delft University of Technology </a>, the
 * Netherlands.
 * <p>
 * See for project information <a
 * href="http://www.simulation.tudelft.nl/dsol/language">www.simulation.tudelft.nl/language </a> <br>
 * License of use: <a href="http://www.gnu.org/copyleft/lesser.html">Lesser General Public License (LGPL)
 * </a>, no warranty
 * 
 * @author <a href="http://www.peter-jacobs.com/index.htm">Peter Jacobs </a>
 * @version $Revision: 1.11 $ $Date: 2005/08/11 05:49:12 $
 * @since 1.5
 */
public final class URLResource
{
    /**
     * constructs a new URLResource.
     */
    private URLResource()
    {
        super();
        // unreachable code
    }

    /**
     * resolves a resource for name.
     * 
     * @param name
     *            the name to search for
     * @return the resolved URL
     */
    public static URL getResource(final String name)
    {
        try
        {
            if (name.startsWith("/"))
            {
                URL url = URLResource.class.getResource(name);
                if (url != null)
                {
                    return url;
                }
                url = Thread.currentThread().getContextClassLoader().getResource(name.substring(1));
                if (url != null)
                {
                    return url;
                }
                File file = new File(name);
                if (file.exists())
                {
                    return new URL("file:" + name);
                }
            }
            else
            {
                if (name.indexOf("@") == -1)
                {
                    return new URL(name);
                }
                // we need authentication
                String temp = name.substring(name.indexOf("//") + 2);
                String userName = temp.substring(0, temp.indexOf(":"));
                String password = temp.substring(temp.indexOf(":") + 1);
                password = password.substring(0, password.indexOf("@"));
                String url = name.substring(0, name.indexOf("//") + 2);
                url = url + name.substring(name.indexOf("@") + 1);
                Authenticator.setDefault(new PasswordAuthenticator(userName, password));
                return new URL(url);
            }
        }
        catch (Exception exception)
        {
            exception = null;
            // We neglect exceptions since we return null
        }
        return null;
    }

    /**
     * returns the reseource as stream.
     * 
     * @param name
     *            the name of the resource
     * @return the inputStream
     */
    public static InputStream getResourceAsStream(final String name)
    {
        try
        {
            URL url = URLResource.getResource(name);
            if (url == null)
            {
                return null;
            }
            return url.openStream();
        }
        catch (Exception exception)
        {
            return null;
        }
    }

    /**
     * A Private password authenticator.
     */
    private static class PasswordAuthenticator extends Authenticator
    {
        /** my username */
        private String userName = null;

        /** my password */
        private String password = null;

        /**
         * constructs a new PasswordAuthenticator.
         * 
         * @param userName
         *            my userName
         * @param password
         *            my passWord
         */
        public PasswordAuthenticator(final String userName, final String password)
        {
            super();
            this.userName = userName;
            this.password = password;
        }

        /**
         * @see java.net.Authenticator#getPasswordAuthentication()
         */
        @Override
        protected PasswordAuthentication getPasswordAuthentication()
        {
            return new PasswordAuthentication(this.userName, this.password.toCharArray());
        }
    }
}
