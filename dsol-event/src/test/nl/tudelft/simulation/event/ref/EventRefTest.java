/*
 * @(#) EventIteratorTest.java Sep 1, 2003 Copyright (c) 2002-2005 Delft
 * University of Technology Jaffalaan 5, 2628 BX Delft, the Netherlands. All
 * rights reserved. This software is proprietary information of Delft University
 * of Technology The code is published under the Lesser General Public License
 */
package nl.tudelft.simulation.event.ref;

import java.rmi.MarshalledObject;

import junit.framework.Assert;
import junit.framework.TestCase;

/**
 * The test script for the reference package. All classes in this package are
 * tested with this test
 * <p>
 * (c) copyright 2002-2005-2004 <a href="http://www.simulation.tudelft.nl">Delft
 * University of Technology </a>, the Netherlands. <br>
 * See for project information <a
 * href="http://www.simulation.tudelft.nl">www.simulation.tudelft.nl </a> <br>
 * License of use: <a href="http://www.gnu.org/copyleft/lesser.html">Lesser
 * General Public License (LGPL) </a>, no warranty.
 * 
 * @author <a href="http://www.peter-jacobs.com">Peter Jacobs </a>
 * @version $Revision: 1.8 $ $Date: 2005/07/04 12:23:02 $
 * @since 1.5
 */
public class EventRefTest extends TestCase
{
    /** TEST_METHOD is the name of the test method */
    public static final String TEST_METHOD = "test";

    /**
     * constructs a new EventIteratorTest.
     */
    public EventRefTest()
    {
        this(TEST_METHOD);
    }

    /**
     * constructs a new EventIteratorTest
     * 
     * @param method the name of the test method
     */
    public EventRefTest(final String method)
    {
        super(method);
    }

    /**
     * tests the classes in the reference class.
     */
    public void test()
    {
        try
        {
            // Test 1: We since we have a pointer to referent, gc should not
            // clean the weakReference

            Object referent = new String("EventIteratorTest");
            /*
             * It is absolutely amazing what you see if you replace the above
             * with the following: Object referent = "EventIteratorTest";
             */

            Reference<Object> reference = new WeakReference<Object>(referent);
            Assert.assertEquals(reference.get(), referent);
            Runtime.getRuntime().gc();
            Assert.assertNotNull(reference.get());

            // Test 2: We since we have a pointer to referent, gc should
            // clean the weakReference
            reference = new WeakReference<Object>(new String(
                    "EventIteratorTest"));
            Runtime.getRuntime().gc();
            Assert.assertNull(reference.get());

            // Test 3: The strong reference...
            reference = new StrongReference<Object>(new String(
                    "EventIteratorTest"));
            Assert.assertNotNull(reference.get());
            Runtime.getRuntime().gc();
            Assert.assertNotNull(reference.get());

            // A Strong one
            new MarshalledObject(new StrongReference<Object>(new Double(12)))
                    .get();

            // A Weak one
            new MarshalledObject(new WeakReference<Object>(new Double(12)))
                    .get();
        } catch (Throwable throwable)
        {
            // runtime exceptions are not appreciated
            throwable.printStackTrace();
            Assert.fail();
        }
    }
}