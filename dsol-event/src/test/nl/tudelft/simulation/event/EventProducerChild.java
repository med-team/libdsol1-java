/*
 * @(#) EventProducerChild.java Sep 1, 2003 Copyright (c) 2002-2005 Delft
 * University of Technology Jaffalaan 5, 2628 BX Delft, the Netherlands. All
 * rights reserved. This software is proprietary information of Delft University
 * of Technology The code is published under the Lesser General Public License
 */
package nl.tudelft.simulation.event;

import java.io.Serializable;

/**
 * The EventProducerChild is an event producer used in JUNIT tests.
 * <p>
 * (c) copyright 2002-2005-2004 <a href="http://www.simulation.tudelft.nl">Delft
 * University of Technology </a>, the Netherlands. <br>
 * See for project information <a
 * href="http://www.simulation.tudelft.nl">www.simulation.tudelft.nl </a> <br>
 * License of use: <a href="http://www.gnu.org/copyleft/lesser.html">Lesser
 * General Public License (LGPL) </a>, no warranty.
 * 
 * @author <a href="http://www.peter-jacobs.com">Peter Jacobs </a>
 * @version $Revision: 1.6 $ $Date: 2005/07/04 12:23:03 $
 * @since 1.5
 */
public class EventProducerChild extends EventProducer implements Serializable
{
    /** EVENT_A is merely a test event */
    public static final EventType EVENT_A = new EventType("EVENT_A");

    /** EVENT_B is merely a test event */
    public static final EventType EVENT_B = new EventType("EVENT_B");

    /** EVENT_C is merely a test event */
    public static final EventType EVENT_C = new EventType("EVENT_C");

    /**
     * constructs a new EventProducerChild
     */
    protected EventProducerChild()
    {
        super();
    }
}